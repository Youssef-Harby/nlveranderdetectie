#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os
from pathlib import Path
from random import shuffle

import numpy as np
from data_loading import data_utils
from h5py import File
from tqdm import tqdm

from nlveranderdetectie.data_loading.errors import LabelsNotFound

logging.getLogger("rasterio").setLevel(logging.WARNING)

def preprocess_data(cfg, require_labels=True):
    """Read raw image/spatial data and convert to hdf5 datasets to be used for training and inference.
       The HDF5 format stores data in a tree like structure with each array being stored under a key,
       but with the possibility of storing lists of keys under a root key. Both the input data and the 
       feature map result are stored in the hdf5 file. The standard format for the files if all layers 
       are present is:
       {xleft}_{ytop}.h5
       root: /
            - rgb
            - label
            - feature_map
            - resized_{block_size}
                - rgb
                - label
                - feature_map
                - feature_map_upscaled
        
        Additional layers like dsm, dtm, cir, superview can be added as well.
        Add the 

    Args:
        cfg (yacs.config.CfgNode): cfg used to determine file paths for loading and saving.
    """
    skiptiles = []
    # read tiles to skip from file
    if os.path.exists(os.path.join(cfg.BASEDIR, 'skiptiles.txt')):
        with open(os.path.join(cfg.BASEDIR, 'skiptiles.txt'), 'r') as f:
            skiptiles = f.read().splitlines()[2:]
            skiptiles = [k.strip() for k in skiptiles]  # remove whitespace
    else:
    # if no skiptiles file, create empty file
        with open(os.path.join(cfg.BASEDIR, 'skiptiles.txt'), 'w') as f:
            f.write("These tiles are skipped during preprocessing because of missing aerial data. " +
                    "Keys are added by the preprocess_dataset.py script, you can add your own below as well.\n")
            f.write("=" * 50)
            f.write("\n")
            pass
    
    filepaths = list(Path(cfg.DATASET.KEYS_FROM).glob("*.tif"))

    keys = [os.path.basename(fp).rsplit('.', 1)[0] for fp in filepaths]
    shuffle(keys)
    
    parsed_folder_name = cfg.DATASET.DATA_SOURCE_FOLDERS.PARSED_DIR
    os.makedirs(os.path.join(cfg.BASEDIR, parsed_folder_name), exist_ok=True)
    sources_to_parse = cfg.MAPPINGS.SAMPLE_CHANNEL_COMBINATIONS[cfg.DATASET.INPUT_STACK]

    additional_group_name = data_utils.h5dataset_name_from_cfg(cfg)

    tbar = tqdm(keys)
    for key in tbar:
        # skip tiles already indentified as to skip.
        if key in skiptiles:
            continue

        tbar.set_description(f'Loading tile {key}')
        if os.path.exists(os.path.join(cfg.BASEDIR, parsed_folder_name, f'{key}.h5')):
            # read it and see if the desired datasets are already there, skip if they are
            with File(os.path.join(cfg.BASEDIR, parsed_folder_name, f'{key}.h5'), 'a') as f:
                target_datasets = []
                if "RGB" in sources_to_parse:
                    target_datasets.append(f"{additional_group_name}/rgb")
                if "CIR" in sources_to_parse:
                    target_datasets.append(f"{additional_group_name}/cir")
                if "DTM" in sources_to_parse:
                    target_datasets.append(f"{additional_group_name}/dtm")
                    target_datasets.append(f"{additional_group_name}/dtm_mean")
                    target_datasets.append(f"{additional_group_name}/dtm_median")
                if "DSM" in sources_to_parse:
                    target_datasets.append(f"{additional_group_name}/dsm")
                
                if require_labels:
                    target_datasets.append(f"{additional_group_name}/label")

                if all([dataset in f.keys() for dataset in target_datasets]):
                    continue
                else:
                    tbar.set_description(f"not all data processed in {key}")

        target_size = (cfg.DATASET.BLOCK_SIZE, cfg.DATASET.BLOCK_SIZE)
        try:
            # convert part of config to dict and pass to unprocessed_input_layers.
            data_paths = data_utils.cfgnode_to_dict(cfg.DATASET.DATA_SOURCE_FOLDERS)
            block_dict = data_utils.load_unprocessed_input_layers(key, data_paths, sources_to_parse, resize_xy=target_size, return_label=require_labels)
        except LabelsNotFound:
            # raises error if there are no labels for the key and return_label is true
            if require_labels:
                print(f"could not find labels for: {key} with basedir: {cfg.BASEDIR}")
                continue

        # if more than 20% is extremely white, its probably one of the coast tiles
        if np.count_nonzero(block_dict['rgb'][:,:,:] > 240) / block_dict['rgb'].size > 0.1:
            # print(block_dict['rgb'][:,:,:], block_dict['rgb'][:,:,:].shape)
            print(f"block has more than 10% pure white color in it. Its probably an edge of the rgb, skip it")
            with open(os.path.join(cfg.BASEDIR, 'skiptiles.txt'), 'a') as f:
                f.write(f"{key}\n")
            continue

        # if its more than 70% water, skip it
        # if 'waterdeel' in block_dict['labels'].keys():
        #     if np.count_nonzero(block_dict['labels']['waterdeel'] == 1) / block_dict['labels']['waterdeel'].size > 0.7:
        #         print(f"block more than 70% water, skip it")
        #         continue
        
        # the pand class needs to be present.
        # if 'pand' not in block_dict['labels'].keys():
        #     continue

        # # if theres only water, skip it.
        # if len(np.unique(block_dict['labels'])) < 2:
        #     print(f"block only has fewer than 2 classes, skip it")
        #     continue
        if require_labels:
            label_array = np.zeros((cfg.DATASET.BLOCK_SIZE, cfg.DATASET.BLOCK_SIZE, cfg.DATASET.NUM_CLASSES), dtype=np.uint8)
            print(f"labels in block: {block_dict['labels'].keys()}")
            for label_name in cfg.DATASET.CLASSES:
                if label_name not in block_dict['labels']:
                    print(f"Missing label: {label_name}")
                else:
                    label_array[:, :, int(cfg.MAPPINGS.CLASS_TO_CLASSIDX[label_name.upper()])] += block_dict['labels'][label_name]

            block_dict['labels'] = data_utils.collapse_onehot(label_array, axis=-1)

        chunksize = 256

        with File(os.path.join(cfg.BASEDIR, parsed_folder_name, f'{key}.h5'), 'a') as f:

            if "rgb" in sources_to_parse and not f"{additional_group_name}/rgb" in f:
                # rgb_array = data_utils.bgr_to_rgb(block_dict['rgb'])  # seems like rasterio reads in bgr so swap them around here.
                f.create_dataset(f"{additional_group_name}/rgb", data=block_dict['rgb'].astype(np.uint8), chunks=(chunksize, chunksize, 3),
                                    compression='lzf')
            if "cir" in sources_to_parse and not f"{additional_group_name}/cir" in f:
                f.create_dataset(f"{additional_group_name}/cir", data=block_dict['cir'].astype(np.uint8), chunks=(chunksize, chunksize, 3),
                                    compression='lzf')
            if "dtm" in sources_to_parse and not f"{additional_group_name}/dtm" in f:
                f.create_dataset(f"{additional_group_name}/dtm", data=block_dict['dtm'].astype(np.float32), chunks=(chunksize, chunksize),
                                    compression='lzf')
                if not f"{additional_group_name}/dtm_mean" in f:
                    f.create_dataset(f"{additional_group_name}/dtm_mean", data=block_dict['dtm_mean'].astype(np.float32))
                if not f"{additional_group_name}/dtm_median" in f:
                    f.create_dataset(f"{additional_group_name}/dtm_median", data=block_dict['dtm_median'].astype(np.float32))

            if "dsm" in sources_to_parse and not f"{additional_group_name}/dsm" in f:
                f.create_dataset(f"{additional_group_name}/dsm", data=block_dict['dsm'].astype(np.float32), chunks=(chunksize, chunksize),
                                    compression='lzf')
            if require_labels and not f"{additional_group_name}/label" in f:
                f.create_dataset(f"{additional_group_name}/label", data=block_dict['labels'].astype(np.uint8), chunks=(chunksize, chunksize),
                                    compression='lzf')
    return None
