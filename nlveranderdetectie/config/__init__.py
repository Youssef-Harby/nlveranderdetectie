from .default import _C as config
from .default import update_config
from .default import get_cfg_defaults
from .sources import DATA_URLS, REGION_DICT