class LabelsNotFound(Exception):
    """Raised when no labels were found for a given key."""
    def __init__(self, *args: object) -> None:
        super().__init__(*args)

class NoPatchFound(Exception):
    """Raised when no patch could be found after many retries."""
    def __init__(self, *args: object) -> None:
        super().__init__(*args)