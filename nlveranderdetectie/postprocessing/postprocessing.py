import argparse
import glob
import os

import numpy as np
import rasterio
from pathlib import Path
from tqdm import tqdm

from nlveranderdetectie.data_loading import data_utils


def postprocess(cfg):
    segmented_filepaths = Path(cfg.BASEDIR) / cfg.DATASET.DATA_SOURCE_FOLDERS.SEGMENTED_DIR
    segmented_filepaths = list(segmented_filepaths.glob("*.tif"))

    for seg_fp in tqdm(segmented_filepaths, desc="generating difference maps"):
        gt_fp_base = os.path.join(
            cfg.BASEDIR,
            cfg.DATASET.DATA_SOURCE_FOLDERS.BGT_DIR,
            seg_fp.stem
        )

        # check first if files exist
        file_exists = []
        for class_n in range(1, 4):
            classname = cfg.MAPPINGS.CLASSIDX_TO_CLASSNAME[class_n]
            diffmap_fp = os.path.join(
                cfg.BASEDIR,
                cfg.DATASET.DATA_SOURCE_FOLDERS.DIFFERENCE_MAP_DIR,
                f"fp_{classname}_{seg_fp.name}",
            )
            diffmap_fn = os.path.join(
                cfg.BASEDIR,
                cfg.DATASET.DATA_SOURCE_FOLDERS.DIFFERENCE_MAP_DIR,
                f"fn_{classname}_{seg_fp.name}",
            )
            if os.path.exists(diffmap_fn) and os.path.exists(diffmap_fp):
                file_exists.append(True)
            else:
                file_exists.append(False)

        if all(file_exists):
            
            continue

        # now load the inputs
        seg_array = rasterio.open(seg_fp).read().squeeze()

        # now actually make the difference map
        for class_n in range(1, 4):
            classname = cfg.MAPPINGS.CLASSIDX_TO_CLASSNAME[class_n]
            gt_fp_class = Path(gt_fp_base) / f"bgt_{classname.lower()}.tif"
            gt_array = rasterio.open(gt_fp_class).read().squeeze()

            # resize GT to match the size of segmented tile
            (s1, s2) = seg_array.shape
            gt_array = data_utils.resize_2d(gt_array, s1=s1, s2=s2)

            diffmap_fp = os.path.join(
                cfg.BASEDIR,
                cfg.DATASET.DATA_SOURCE_FOLDERS.DIFFERENCE_MAP_DIR,
                f"fp_{classname}_" + os.path.basename(seg_fp),
            )
            diffmap_fn = os.path.join(
                cfg.BASEDIR,
                cfg.DATASET.DATA_SOURCE_FOLDERS.DIFFERENCE_MAP_DIR,
                f"fn_{classname}_" + os.path.basename(seg_fp),
            )

            mask_fp = np.zeros((gt_array.shape))
            mask_fn = np.zeros((gt_array.shape))

            mask_fp[(gt_array != 1) & (seg_array == class_n)] = 1
            mask_fn[(gt_array == 1) & (seg_array != class_n)] = 1

            x, y = os.path.basename(seg_fp)[:-4].split("_")
            x, y = int(x), int(y)

            data_utils.save_block_as_geotiff(
                diffmap_fp, mask_fp, "28992", 10000, x, y, make_bool_mask=True
            )
            data_utils.save_block_as_geotiff(
                diffmap_fn, mask_fn, "28992", 10000, x, y, make_bool_mask=True
            )
