import argparse
import glob
import os
import pprint
import sys
from collections import defaultdict

import h5py
from h5py import File
from pynput.keyboard import Key, Listener
from tqdm import tqdm

from nlveranderdetectie.config import get_cfg_defaults, update_config
sys.path.insert(1, os.path.join(sys.path[0], '..'))


class DatasetChecker():
    """Runs through dataset of hdf5 files and counts what sources in what shapes are present. 
    Useful for inspecting a hdf5 dataset folder that you suspect contains partial or missing entries or 
    that if you don't know the sizes of the data.
    """

    def __init__(self):
        self.sets = []
        self.names = []

    def format(self):
        return {self.names[i]: self.sets[i].shape for i in range(len(self.sets))}

    def __call__(self, name, node):
        if isinstance(node, h5py.Dataset):
            self.sets.append(node)
            self.names.append(name)
        return None


def verify(cfg):
    parsed_files = glob.glob(os.path.join(cfg.DATASET.DATA_SOURCE_FOLDERS.PARSED_DIR) + "/*.h5")
    result_dict = defaultdict(lambda: 0)
    print("starting to collect data on hdf5 files in the dataset, ctrl-C to halt execution, press p to see progress so far.")
    print("=" * 50)

    def show_dict(key):
        if type(key) == Key:
            return
        else:
            if key.char == 'p':
                pprint.pprint(result_dict)

    try:
        with Listener(on_press=show_dict) as listener:
            for i, fp in enumerate(tqdm(parsed_files, desc='collecting keys from hdf5 files.')):
                key = os.path.basename(fp.rsplit(".")[0])
                with File(fp, 'r') as f:

                    dsc = DatasetChecker()
                    f.visititems(dsc)
                    res = dsc.format()
                    for key, value in res.items():

                        result_dict[f'{key}: {value}'] += 1
            listener.join()
    except KeyboardInterrupt:
        print(f"execution interrupted at iteration {i}/{len(parsed_files)}, results so far: ")

    pprint.pprint(result_dict)


def parse_args():
    parser = argparse.ArgumentParser(description='Train segmentation network')
    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        required=True,
                        type=str)
    parser.add_argument('--opts',
                        help="Modify config options using the command-line",
                        default=[],
                        nargs=argparse.REMAINDER)
    args = parser.parse_args()
    cfg = get_cfg_defaults()
    cfg = update_config(cfg, args, freeze=False)
    return cfg


if __name__ == "__main__":
    cfg = parse_args()
    verify(cfg)
