import argparse
import glob
import os
import subprocess
import sys

import data_loading.data_utils as data_utils
import numpy as np
from config import get_cfg_defaults, update_config
from h5py import File
from tqdm import tqdm

sys.path.insert(1, os.path.join(sys.path[0], '..'))


def combine(cfg):
    parsed_files = glob.glob(os.path.join(cfg.DATASET.DATA_SOURCE_FOLDERS.PARSED_DIR) + "/*.h5")
    parsed_files = parsed_files

    os.makedirs(os.path.join(cfg.BASEDIR, 'geotiff', cfg.target_layer), exist_ok=True)
    target_folder = os.path.join(cfg.BASEDIR, 'geotiff', cfg.target_layer)

    for fp in tqdm(parsed_files, desc='converting h5 files to geotiff'):
        target_path = os.path.join(target_folder, os.path.basename(fp).rsplit('.')[0] + ".tif")
        if not os.path.exists(target_path):
            with File(fp, 'r') as f:
                key = os.path.basename(fp.rsplit(".")[0])
                epsg = "28992"
                x_left = key.split("_")[0]
                y_top = key.split("_")[1]
                try:
                    fm = f[cfg.target_layer]
                    block_size = fm.shape[1]
                except KeyError:
                    # no feature map for this particular block
                    continue

                # skip the blocks that have 2 dimension feature maps, something's wrong with them
                if np.ndim(fm) > 2:
                    fm = np.argmax(fm, axis=-1)
                    data_utils.save_block_as_geotiff(
                        target_path, fm, epsg, block_size, x_left, y_top, make_bool_mask=False)

                # fix ns pixel, not needed anymore
                # target_path_tmp = os.path.join(target_folder, "tmp_" + os.path.basename(fp).rsplit('.')[0] + ".tif")
                # subprocess.call(['gdalwarp', '-t_srs', f'EPSG:{epsg}', '-overwrite', target_path, target_path_tmp], stdout=subprocess.DEVNULL)
                # shutil.move(target_path_tmp, target_path)

    print("building virtual mosaic from tiff files using gdalbuildvrt")
    subprocess.call(
        f'gdalbuildvrt {target_folder}/mosaic.vrt {target_folder}/*.tif', shell=True, stdout=subprocess.DEVNULL)

    if cfg.create_single_tiff:
        print('Converting virtual mosaic to tif file')
        subprocess.call(
            f'gdal_translate -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" {target_folder}/mosaic.vrt {target_folder}/mosaic.tif', shell=True)


def parse_args():
    parser = argparse.ArgumentParser(description='Train segmentation network')
    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        required=True,
                        type=str)
    parser.add_argument('--target-layer',
                        help='experiment configure file name',
                        required=True,
                        type=str)
    parser.add_argument('--create-single-tiff',
                        help='experiment configure file name',
                        default=False,
                        type=bool)
    parser.add_argument('--opts',
                        help="Modify config options using the command-line",
                        default=[],
                        nargs=argparse.REMAINDER)
    args = parser.parse_args()
    cfg = get_cfg_defaults()
    cfg = update_config(cfg, args, freeze=False)
    cfg.target_layer = args.target_layer
    cfg.create_single_tiff = args.create_single_tiff
    return cfg


if __name__ == "__main__":
    cfg = parse_args()
    combine(cfg)
