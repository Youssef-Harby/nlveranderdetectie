import argparse
import logging
import os

from nlveranderdetectie.config import get_cfg_defaults, update_config
from nlveranderdetectie.data_loading import data_utils
from nlveranderdetectie.training import BGTsegmentation


def parse_args():
    parser = argparse.ArgumentParser(description='Train segmentation network')
    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        required=True,
                        type=str)
    parser.add_argument("--overwrite-parsing", default=False, action="store_true",
                        help="Overwrite existing feature maps")
    parser.add_argument("--add-upscaled", default=False, action="store_true",
                        help="add upscaled version of feature map")
    parser.add_argument("--no-labels", default=False, action="store_true",
                        help="don't process & save labels")
    parser.add_argument('--opts',
                        help="Modify config options using the command-line",
                        default=[],
                        nargs=argparse.REMAINDER)
    args = parser.parse_args()
    cfg = get_cfg_defaults()
    cfg = update_config(cfg, args, freeze=False)
    return cfg, args


def main():
    logging.basicConfig(level=logging.INFO)
    cfg, args = parse_args()

    # load module with model
    pl_module = BGTsegmentation(cfg)
    model = pl_module.model
    model.eval()

    model = model.to("cuda:0")
    h5_folder = os.path.join(cfg.DATASET.DATA_SOURCE_FOLDERS.PARSED_DIR)
    data_utils.generate_feature_maps(cfg, model, h5_folder, h5_folder, overwrite=args.overwrite_parsing,
                                     require_labels=not(args.no_labels), add_upscaled=args.add_upscaled)


if __name__ == "__main__":
    logging.getLogger('fiona').setLevel(logging.WARNING)
    logging.getLogger('shapely').setLevel(logging.WARNING)
    logging.basicConfig(level=logging.DEBUG)
    main()
