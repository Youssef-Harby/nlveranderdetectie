import argparse
import logging

from nlveranderdetectie.config import get_cfg_defaults, update_config
from nlveranderdetectie.preprocessing import h5writer


def parse_args():
    parser = argparse.ArgumentParser(description='preprocess data to h5 dataset')
    parser.add_argument('--cfg',
                        help='experiment configure file name',
                        required=True,
                        type=str)
    parser.add_argument("--no-labels", default=False, action="store_true",
                        help="Flag to do something")
    parser.add_argument('--opts',
                        help="Modify config options using the command-line",
                        default=[],
                        nargs=argparse.REMAINDER)
    args = parser.parse_args()
    cfg = get_cfg_defaults()
    cfg = update_config(cfg, args, freeze=False)
    cfg.no_labels = args.no_labels
    return cfg


def main():
    logging.basicConfig(level=logging.INFO)
    cfg = parse_args()
    h5writer.preprocess_data(cfg, require_labels=not(cfg.no_labels))


if __name__ == "__main__":
    logging.getLogger('fiona').setLevel(logging.WARNING)
    logging.getLogger('shapely').setLevel(logging.WARNING)
    logging.basicConfig(level=logging.DEBUG)
    main()
