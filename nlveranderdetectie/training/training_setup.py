import os
import torch
import wandb
from torch.utils.data import DataLoader
import lightning.pytorch as pl

from lightning.pytorch.loggers import WandbLogger
from lightning.pytorch.callbacks import ModelCheckpoint
from lightning.pytorch.strategies import DDPStrategy

from nlveranderdetectie.data_loading.datasets import BGTSEG
from nlveranderdetectie.training import BGTsegmentationModel

def train(cfg):
    torch.manual_seed(cfg.TRAIN.SEED)

    dataset_train = BGTSEG(cfg, 'train')
    dataset_val = BGTSEG(cfg, 'val')

    train_loader = DataLoader(dataset_train, batch_size=cfg.TRAIN.BATCH_SIZE,
                              num_workers=cfg.WORKERS, pin_memory=True)
    val_loader = DataLoader(dataset_val, batch_size=cfg.TEST.BATCH_SIZE,
                            num_workers=cfg.WORKERS, pin_memory=True)

    # wandb logger
    # wandb.init(dir=os.path.join(cfg.BASEDIR), config=cfg)
    wandb_logger = WandbLogger(name=cfg.CHECKNAME, save_dir=os.path.join(
        cfg.BASEDIR), config=cfg, sync_tensorboard=False, project="BGTseg_v3")

    # model
    lightning_model = BGTsegmentationModel(cfg)

    # watch gradients and parameters using wandb
    wandb_logger.watch(lightning_model.model, log='all')

    os.makedirs(os.path.join(wandb.run.dir, 'models'), exist_ok=True)
    model_save_path = os.path.join(wandb.run.dir, 'models')

    checkpoint_callback = ModelCheckpoint(
        monitor="dice",
        dirpath=model_save_path,
        filename="transf_seg-{epoch:02d}-{dice:.2f}",
        save_top_k=3,
        mode="max",
    )

    # Explicitly specify the process group backend, default is nccl, but that doesnt work on windows
    # (where I am training now). If you are training on linux or using a docker container, you can
    # switch the backend back to nccl
    ddp = DDPStrategy(process_group_backend="gloo")

    # training
    trainer = pl.Trainer(num_nodes=1,
                         precision='16-mixed',
                         limit_train_batches=1.0,
                         limit_val_batches=1.0,
                         limit_test_batches=1.0,
                         accumulate_grad_batches=cfg.TRAIN.ACCUMULATE_GRAD_BATCHES,
                         log_every_n_steps=1,
                         logger=wandb_logger,
                         max_epochs=100,
                         accelerator='gpu',
                         strategy=ddp,
                         callbacks=[checkpoint_callback])

    if cfg.TRAIN.VALIDATION_ONLY:
        print(f"VALIDATION_ONLY SET TO {cfg.TRAIN.VALIDATION_ONLY}")
        print(f"    skipping training step and doing a single validation run instead")
        if cfg.TRAIN.RESUME != "":
            lightning_model = lightning_model.load_from_checkpoint(cfg.TRAIN.RESUME)
            trainer.validate(lightning_model, dataloaders=val_loader)
        else:
            trainer.validate(lightning_model, dataloaders=val_loader)
    else:
        if cfg.TRAIN.RESUME != "":
            lightning_model = lightning_model.load_from_checkpoint(cfg.TRAIN.RESUME)
            trainer.fit(lightning_model, train_loader, val_loader)
        else:
            trainer.fit(lightning_model, train_loader, val_loader)


if __name__ == "__main__":
    train()
