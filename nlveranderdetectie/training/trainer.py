import os
import sys

import numpy as np
import lightning.pytorch as pl
import torch
import torchvision
import wandb
from torch.nn import functional as F

# segformer model
from transformers import SegformerForSemanticSegmentation

import segmentation_models_pytorch as smp

from nlveranderdetectie.data_loading import data_utils

class BGTsegmentationModel(pl.LightningModule):
    def __init__(self, cfg):
        """Pytorch Lightning trainer module, this module defines all the standard functions for a pt lightning trainer.
        Args:
            cfg (yacs.config.CfgNode): cfg used for training setup options such as: 
            optimizer parameters, paths to model files, validation options etc.

        """
        super().__init__()
        self.cfg = cfg
        # we want to skip on_validation_epoch_end the first time if we're doing a regular training run.
        if cfg.TRAIN.VALIDATION_ONLY:
            self.skip_first = False
        else:
            self.skip_first = True

        self.save_hyperparameters()
        if cfg.MODEL.NAME == 'deeplabv3plus':
            self.model = smp.DeepLabV3Plus(
                encoder_name = self.cfg.MODEL.BACKBONE,
                in_channels = self.cfg.DATASET.N_INPUTCHANNELS,
                classes = cfg.DATASET.NUM_CLASSES,
                encoder_weights="imagenet"
            )
        elif cfg.MODEL.NAME == 'manet':
            self.model = smp.MAnet(
                encoder_name = self.cfg.MODEL.BACKBONE,
                in_channels = self.cfg.DATASET.N_INPUTCHANNELS,
                classes = cfg.DATASET.NUM_CLASSES,
                encoder_weights="imagenet"
            )
        elif cfg.MODEL.NAME == 'segformer':
            # additional segformer options:
            # nvidia/segformer-b1-finetuned-cityscapes-1024-1024
            # nvidia/segformer-b2-finetuned-cityscapes-1024-1024
            # nvidia/segformer-b3-finetuned-cityscapes-1024-1024
            # nvidia/segformer-b4-finetuned-cityscapes-1024-1024
            # nvidia/segformer-b5-finetuned-cityscapes-1024-1024
            # and all the other variants available from nvidia https://huggingface.co/nvidia
            self.model = SegformerForSemanticSegmentation.from_pretrained("nvidia/segformer-b1-finetuned-cityscapes-1024-1024")
            
            # overwrite base classifier with one specific to our amount of required channels
            in_channels = self.model.decode_head.classifier.in_channels
            self.model.decode_head.classifier = torch.nn.Conv2d(in_channels, cfg.DATASET.NUM_CLASSES, kernel_size=(1,1), stride=(1,1))
        else:
            raise NotImplementedError(f"no implementation for model: {cfg.MODEL.NAME}")
        
        # init losses, taken from https://smp.readthedocs.io/en/latest/losses.html
        if self.cfg.LOSS.LOSS_TYPE.lower() == "dice":
            self.loss_function = smp.losses.DiceLoss(mode='multiclass')
        elif self.cfg.LOSS.LOSS_TYPE.lower() == "tverskyloss":
            # Tversky loss for image segmentation task. Where FP and FN is weighted by alpha and beta params. 
            # With alpha == beta == 0.5, this loss becomes equal DiceLoss. It supports binary, multiclass and multilabel cases
            # alpha – Weight constant that penalize model for FPs (False Positives)
            # beta – Weight constant that penalize model for FNs (False Negatives)
            self.loss_function = smp.losses.TverskyLoss(mode='multiclass',
                                                        alpha=0.5,
                                                        beta=0.5)
        elif self.cfg.LOSS.LOSS_TYPE.lower() == 'ce':
            self.loss_function = F.cross_entropy
        elif self.cfg.LOSS.LOSS_TYPE.lower() == 'mse':
            self.loss_function = F.mse_loss
        else:
            print(f"loss function: {self.cfg.LOSS.LOSS_TYPE} not defined.")
            sys.exit(0)
        

    def forward(self, x):
        """Forward pass of the model, differs if the type of the model changes. Used in training_step and validation_step."""
        if self.cfg.MODEL.NAME == 'deeplabv3plus':
            pred = self.model(x)['out']
        elif self.cfg.MODEL.NAME == "segformer":
            inputs = {
                "pixel_values": x
            }
            outputs = self.model(**inputs)
            pred = data_utils.resize_torch(outputs.logits, size=self.cfg.TRAIN.IMAGE_SIZE)
        elif self.cfg.MODEL.NAME == "manet":
            pred = self.model(x)
        else:
            raise NotImplementedError(f"no forward implemented for model: '{self.cfg.MODEL.NAME}'. Please check you config YAML file.")

        return pred

    def configure_optimizers(self):
        if self.cfg.MODEL.NAME == "deeplabv3plus":
            if self.cfg.TRAIN.OPTIMIZER == "adam":
                optimizer = torch.optim.AdamW(self.parameters(), lr=self.cfg.TRAIN.LR, amsgrad=self.cfg.TRAIN.AMSGRAD, betas=self.cfg.TRAIN.BETAS, eps=self.cfg.TRAIN.EPSILON)
            elif self.cfg.TRAIN.OPTIMIZER == "sgd":
                optimizer = torch.optim.SGD(self.parameters(), lr=self.cfg.TRAIN.LR, momentum=self.cfg.TRAIN.MOMENTUM, nesterov=self.cfg.TRAIN.NESTEROV, weight_decay=self.cfg.TRAIN.WEIGHT_DECAY)
            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=self.cfg.TRAIN.FACTOR, patience=self.cfg.TRAIN.PATIENCE, verbose=True)
            return {"optimizer": optimizer, "lr_scheduler": scheduler, "monitor": "training/train_loss_epoch"}

        elif self.cfg.MODEL.NAME == 'segformer':
            if self.cfg.TRAIN.OPTIMIZER == "adam":
                optimizer = torch.optim.AdamW(self.parameters(), lr=self.cfg.TRAIN.LR, amsgrad=self.cfg.TRAIN.AMSGRAD, betas=self.cfg.TRAIN.BETAS, eps=self.cfg.TRAIN.EPSILON)
            elif self.cfg.TRAIN.OPTIMIZER == "sgd":
                optimizer = torch.optim.SGD(self.parameters(), lr=self.cfg.TRAIN.LR, momentum=self.cfg.TRAIN.MOMENTUM, nesterov=self.cfg.TRAIN.NESTEROV, weight_decay=self.cfg.TRAIN.WEIGHT_DECAY)

            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=self.cfg.TRAIN.FACTOR, patience=self.cfg.TRAIN.PATIENCE, verbose=True)
            return {"optimizer": optimizer, "lr_scheduler": scheduler, "monitor": "training/train_loss_epoch"}
        elif self.cfg.MODEL.NAME == 'manet':
            if self.cfg.TRAIN.OPTIMIZER == "adam":
                optimizer =  torch.optim.AdamW(self.parameters(), lr=self.cfg.TRAIN.LR, amsgrad=self.cfg.TRAIN.AMSGRAD, betas=self.cfg.TRAIN.BETAS, eps=self.cfg.TRAIN.EPSILON)
            elif self.cfg.TRAIN.OPTIMIZER == "sgd":
                optimizer =  torch.optim.SGD(self.parameters(), lr=self.cfg.TRAIN.LR, momentum=self.cfg.TRAIN.MOMENTUM, nesterov=self.cfg.TRAIN.NESTEROV, weight_decay=self.cfg.TRAIN.WEIGHT_DECAY)
            
            scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=self.cfg.TRAIN.FACTOR, patience=self.cfg.TRAIN.PATIENCE, verbose=True)
            return {"optimizer": optimizer, "lr_scheduler": scheduler, "monitor": "training/train_loss_epoch"}
        else:
            return NotImplementedError(f"no optimizers implemented for model type: {self.cfg.MODEL.NAME}")

    def training_step(self, train_batch, batch_idx):
        x = train_batch['image'].contiguous()
        y = train_batch['label'].long().contiguous()

        # self(x) calls the __call__ method which calls the forward method above
        pred = self(x)

        loss = self.loss_function(pred, y)

        self.log("training/train_loss", loss, prog_bar=True, logger=True, on_epoch=True, on_step=True)

        # training_batch_idx = (self.current_epoch * self.num_batches) + batch_idx # untested
        training_batch_idx = batch_idx

        pred = torch.argmax(pred, 1)
        pred = pred.reshape(pred.shape[0], 1, pred.shape[1], pred.shape[2])
        y = y.reshape(y.shape[0], 1, y.shape[1], y.shape[2])

        # TODO: make this a parameter of config
        samples_to_log = 3
        # if the batch size is smaller than samples to log, set it to batch size
        if pred.shape[0] < samples_to_log:
            samples_to_log = pred.shape[0]

        images_pred_list = []
        gt_list = []

        # loop over images, decode them, then stack em to pass to make_grid
        for sample_idx in range(samples_to_log):
            pred_single = data_utils.decode_segmap(pred[sample_idx], self.cfg)
            images_pred_list.append(pred_single)
            y_single = data_utils.decode_segmap(y[sample_idx], self.cfg)
            gt_list.append(y_single)

        # only log with an interval, also only log for optimizer 0
        log_this_step = False
        if batch_idx % self.cfg.TRAIN.IMAGE_LOG_FREQ == 0:
            log_this_step = True

        if self.cfg.DATASET.INPUT_STACK == "RGB":
            rgb = x
        elif self.cfg.DATASET.INPUT_STACK == "RGB_FEAT2000":
            rgb = x[:, :3, :, :]
            if log_this_step:
                feat2000 = torch.argmax(x[:, 3:, :, :], dim=1)
                feat2000_list = []
                for sample_idx in range(samples_to_log):
                    feat_single = data_utils.decode_segmap(feat2000[sample_idx], self.cfg)
                    feat2000_list.append(feat_single)

                feat_images = [wandb.Image(feat2000, caption='feat2000')
                               for feat2000 in feat2000_list]

        if log_this_step:
            # using normalize here instead of the actual standardization values changes the image slightly
            # but this only means the logged images look slightly different, this is not actually a problem.
            gt_list = [im.squeeze().cpu().numpy() for im in gt_list]
            images_pred_list = [im.squeeze().cpu().numpy() for im in images_pred_list]

            grid = [wandb.Image(rgb, caption='rgb') for rgb in rgb[:samples_to_log]]
            grid_pred = [wandb.Image(pred, caption='segmentation') for pred in images_pred_list]
            grid_true = [wandb.Image(gt, caption='ground_truth') for gt in gt_list]

            for i in range(len(grid)):
                if self.cfg.DATASET.INPUT_STACK == "RGB":
                    wandb.log(
                        {f"training/sample_{i}": [grid[i], grid_pred[i], grid_true[i]], "global_step": self.global_step})
                elif self.cfg.DATASET.INPUT_STACK == "RGB_FEAT2000":
                    wandb.log({f"training/sample_{i}": [grid[i], grid_pred[i],
                              grid_true[i], feat_images[i]], "global_step": self.global_step})

        return loss

    def segment_and_log(self, key):
        print(f"segmenting key: {key}\n")
        if self.cfg.TRAIN.NO_VALIDATION:
            print(f"NO_VALIDATION SET TO {self.cfg.TRAIN.VALIDATION_ONLY}")
            print(f"    skipping validation step and continuing to the next epoch")
            return

        additional_group_name = data_utils.h5dataset_name_from_cfg(self.cfg)

        sources_to_use = self.cfg.MAPPINGS.SAMPLE_CHANNEL_COMBINATIONS[self.cfg.DATASET.INPUT_STACK]
        h5_filepath = os.path.join(
            self.cfg.BASEDIR, self.cfg.DATASET.DATA_SOURCE_FOLDERS.PARSED_DIR, f"{key}.h5")
        try:
            if self.cfg.TRAIN.DATA_FROM_GEOTIFFS:
                image, label = data_utils.load_unprocessed_stacked(self.cfg, key)
                # load unprocessed_stacked does not load a
                # rest of the method expects a collapsed label, however we need to add an extra 'background class' layer
                # to the block to get the right indices
                # label = np.stack(np.array(label.shape[0], label.shape[1], dtype=np.float32), label, axis=-1)
                fixed_label = np.zeros(
                    (label.shape[0], label.shape[1], self.cfg.DATASET.NUM_CLASSES), dtype=np.float32)
                # classes are read in order by load_unprocessed_stacked, here we transform that order into the actual classmapping
                # as defined in the cfg.
                for i, c in enumerate(self.cfg.DATASET.CLASSES):
                    c_index = self.cfg.MAPPINGS.CLASS_TO_CLASSIDX[c.upper()]
                    fixed_label[:, :, c_index] = label[:, :, i]

                # now we can collapse the onehot encoded label into the label used for calculating the metrics
                label = data_utils.collapse_onehot(fixed_label, axis=-1)
                del fixed_label

            else:
                image, label = data_utils.load_h5_block(h5_filepath, sources_to_use, resize_xy=(
                    self.cfg.DATASET.BLOCK_SIZE, self.cfg.DATASET.BLOCK_SIZE), group_name=additional_group_name)
        except KeyError as e:
            if self.cfg.TRAIN.DATA_FROM_GEOTIFFS:
                print(f"could not load key : {key}, from filepath {h5_filepath.replace('.h5', '.tif')}. caught error: {e}")
            else:
                print(f"could not load key : {key}, from filepath {h5_filepath}. caught error: {e}")
            print("continuing to next key.")
            return

        segmented_tile = data_utils.segment_tile(self,
                                                 image,
                                                 self.cfg.DATASET.NUM_CLASSES,
                                                 self.cfg.TRAIN.IMAGE_SIZE,
                                                 self.cfg.TEST.BATCH_SIZE,
                                                 self.cfg.DATASET.INPUT_STACK,
                                                 collapse=False).detach().cpu().float()

        label = torch.from_numpy(label).long()
        loss = self.loss_function(segmented_tile.unsqueeze(0), label.unsqueeze(0))

        self.logger.log_metrics({f"validation/{key}/{self.cfg.LOSS.LOSS_TYPE}": loss}, step=self.global_step)

        segmented_tile = np.argmax(segmented_tile, axis=0)
        tp, fp, fn, tn = smp.metrics.get_stats(segmented_tile, label, mode='multiclass', num_classes=self.cfg.DATASET.NUM_CLASSES)

        iou_score = smp.metrics.iou_score(tp, fp, fn, tn, reduction="macro-imagewise")
        f1_score = smp.metrics.f1_score(tp, fp, fn, tn, reduction="macro-imagewise")
        f2_score = smp.metrics.fbeta_score(tp, fp, fn, tn, beta=2, reduction="macro-imagewise")
        accuracy = smp.metrics.accuracy(tp, fp, fn, tn, reduction="macro-imagewise")
        recall = smp.metrics.recall(tp, fp, fn, tn, reduction="macro-imagewise")
        precision = smp.metrics.precision(tp, fp, fn, tn, reduction="macro-imagewise")
        sensitivity = smp.metrics.sensitivity(tp, fp, fn, tn, reduction="macro-imagewise")
        specificity = smp.metrics.specificity(tp, fp, fn, tn, reduction="macro-imagewise")
        dice = (2 * iou_score) / (iou_score + 1)

        self.logger.log_metrics({f"validation/{key}/dice": dice,
                                  f"validation/{key}/precision": precision,
                                  f"validation/{key}/recall": recall,
                                  f"validation/{key}/accuracy": accuracy,
                                  f"validation/{key}/f1score": f1_score,
                                  f"validation/{key}/f2score": f2_score,
                                  f"validation/{key}/sensitivity": sensitivity,
                                  f"validation/{key}/specificity": specificity,
                                  f"validation/{key}/iou": iou_score}, step=self.global_step)
        if self.cfg.DATASET.INPUT_STACK == "RGB":
            image = image[:, :, :3]

        if self.cfg.DATASET.INPUT_STACK == "RGB_FEAT2000":
            image = image[:, :, :3]

        segmented_tile = data_utils.decode_segmap(segmented_tile, self.cfg)
        label_rgb = data_utils.decode_segmap(label, self.cfg).squeeze().cpu().numpy()
        image = torchvision.utils.make_grid(torch.tensor(
            image).float(), normalize=True).float().cpu().numpy()

        # resize the images to smaller size so that they can be viewed in wandb without crashing your browser :P
        if self.cfg.TRAIN.VALIDATION_TILE_RESIZING:
            label_rgb = data_utils.resize_2d(
                label_rgb, s1=self.cfg.TRAIN.VALIDATION_TILE_RESIZING_RES, s2=self.cfg.TRAIN.VALIDATION_TILE_RESIZING_RES)
            image = data_utils.resize_2d(
                image, s1=self.cfg.TRAIN.VALIDATION_TILE_RESIZING_RES, s2=self.cfg.TRAIN.VALIDATION_TILE_RESIZING_RES)
            segmented_tile = data_utils.resize_2d(segmented_tile.squeeze().cpu().numpy(
            ), s1=self.cfg.TRAIN.VALIDATION_TILE_RESIZING_RES, s2=self.cfg.TRAIN.VALIDATION_TILE_RESIZING_RES)

        wandb.log({f"validation/tiles/{key}": [
            wandb.Image(image, caption=f'rgb'),
            wandb.Image(label_rgb, caption=f'ground truth'),
            wandb.Image(segmented_tile, caption=f'segmentation')
        ], "global_step": self.global_step})

        del label_rgb, image
        return dice, precision, recall, accuracy, f1_score, iou_score

    def on_validation_epoch_end(self):
        # so that this function is not ran during the sanity check at the start of training
        if self.global_step == 0 and self.skip_first:
            return

        to_segment = [key for key in self.cfg.DATASET.VALIDATION_KEYS
                      if os.path.exists(os.path.join(self.cfg.BASEDIR, self.cfg.DATASET.DATA_SOURCE_FOLDERS.PARSED_DIR, f"{key}.h5"))]
        to_segment = [key for key in self.cfg.DATASET.VALIDATION_KEYS
                      if os.path.exists(os.path.join(self.cfg.BASEDIR, self.cfg.DATASET.KEYS_FROM, f"{key}.tif"))]
        to_segment = list(set(to_segment))
        # easy setting to skip some validation tiles if desired
        to_segment = to_segment[:self.cfg.DATASET.MAX_VAL_TILES]

        total_dice = []
        total_precision = []
        total_recall = []
        total_accuracy = []
        total_fscore = []
        total_jaccard = []
        for key in to_segment:
            # no grad probably fixes memory errors
            with torch.no_grad():
                dice, precision, recall, accuracy, fscore, jaccard = self.segment_and_log(key)

                total_dice.append(dice)
                total_precision.append(precision)
                total_recall.append(recall)
                total_accuracy.append(accuracy)
                total_fscore.append(fscore)
                total_jaccard.append(jaccard)

        self.logger.log_metrics({"validation/dice": np.mean(total_dice),
                          "validation/precision": np.mean(total_precision),
                          "validation/recall": np.mean(total_recall),
                          "validation/accuracy": np.mean(total_accuracy),
                          "validation/fscore": np.mean(total_fscore),
                          "validation/jaccard": np.mean(total_jaccard)})

        self.log(f"dice", np.mean(total_dice), sync_dist=True)

    def validation_step(self, val_batch, batch_idx):
        x = val_batch['image'].contiguous()
        y = val_batch['label'].long().contiguous()

        # self(x) calls the __call__ method which calls the forward method above
        pred = self(x)

        loss = self.loss_function(pred, y)
        self.log('validation/random_sample_val_loss', loss, sync_dist=True)

    def test_step(self):
        """Test the validation configuration by running a single fulltile validation step
        """
        self.on_validation_epoch_end()
