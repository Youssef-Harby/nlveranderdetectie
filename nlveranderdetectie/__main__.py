import argparse
import datetime
import sys
from .config import get_cfg_defaults, update_config
from .data_retrieval import query_data
from .training import train
from .training import BGTsegmentationModel
from .postprocessing import postprocess
from .data_loading.data_utils import segment_folder
from .config.sources import REGION_DICT


def parse_args():
    parser = argparse.ArgumentParser(description="Train segmentation network")
    parser.add_argument(
        "--cfg", help="experiment configure file name", required=True, type=str
    )
    parser.add_argument(
        "--mode",
        help="experiment configure file name",
        required=True,
        choices=["query", "train", "segment", "postprocess"],
        type=str,
    )
    parser.add_argument(
        "--opts",
        help="Modify config options using the command-line",
        default=[],
        nargs=argparse.REMAINDER,
    )

    # optional arguments for querying data, not saved in the global config but directly here
    parser.add_argument(
        "--no-ssl-check",
        action="store_true",
        default=False,
        help="Ignore ssl verification checks, turn on if you get self signed certificate problems.",
    )
    parser.add_argument(
        "--refresh-all",
        action="store_true",
        default=False,
        help="Overwrite existing copies of already queried data.",
    )
    parser.add_argument(
        "--data-version",
        default="",
        help="What year of data to query or version in case of dsm/dtm.",
    )
    parser.add_argument(
        "--data-type",
        default="",
        choices=["rgb", "cir", "dsm", "dtm", "bgt"],
        help="what type of data to query",
    )
    parser.add_argument(
        "--named-area",
        type=str,
        default="",
        help="named region to query, see sources.py in the config for a full list.",
    )
    parser.add_argument(
        "--area-from-file",
        default="",
        help="read coordinates from file containing x1,y1,x2,y2 per line",
    )

    # optional arguments for segmentation, this argument is parsed by the update_config function
    parser.add_argument(
        "--process-subset",
        help="Process subset of keys in basedir directory",
        default=False,
        nargs=argparse.REMAINDER,
    )
    
    parser.add_argument(
        "--bgt-timestamp",
        default=None,
        type=str,
        help="Timestamp to select BGT attributes that where active at that moment.",
    )

    args = parser.parse_args()

    if args.mode == "query":
        if args.data_version == "" or args.data_type == "":
            print(
                "please specify 'data-version' and 'data-type' arguments when using the query mode."
            )
            sys.exit(0)

        if args.named_area == "" and args.area_from_file == "":
            print(
                "please specify 'named-area' or 'coordinates-from-file' arguments when using the query mode."
            )
            print(
                "additional areas can be added by changing the sources.py file in the config folder."
            )
            print("current available options are:\n")
            print(list(REGION_DICT.keys()))
            sys.exit(0)

    # query specific args
    query_args = {
        "no_ssl_check": args.no_ssl_check,
        "data_version": args.data_version,
        "refresh_all": args.refresh_all,
        "data_version": args.data_version,
        "data_type": args.data_type,
        "area_from_file": args.area_from_file,
        "named_area": args.named_area,
        "bgt_timestamp" :args.bgt_timestamp
    }

    # one dict to contain all extra args
    extra_args = {"query_args": query_args}

    cfg = get_cfg_defaults()
    cfg = update_config(cfg, args, freeze=False)
    if cfg.CHECKNAME is None:
        cfg.CHECKNAME = (
            str(cfg.MODEL.NAME)
            + "-run-"
            + datetime.datetime.now().strftime(format="%Y-%m-%d_%H-%M-%S")
        )

    cfg.freeze()
    return cfg, args.mode, extra_args


def get_dataset_path(cfg, data_type):
    key = f"{data_type.upper()}_DIR"
    dir = cfg.DATASET.DATA_SOURCE_FOLDERS[key]
    return dir


def main():
    cfg, mode, extra_args = parse_args()

    if mode == "query":
        print("querying data from pdok!")
        query_args = extra_args["query_args"]
        save_dir = get_dataset_path(cfg, query_args["data_type"])
        query_data(
            save_dir,
            query_args["data_type"],
            cfg.DATASET.CLASSES,
            query_args["data_version"],
            query_args["named_area"],
            query_args["area_from_file"],
            query_args["bgt_timestamp"],
            no_ssl_check=query_args["no_ssl_check"],
            refresh_all=query_args["refresh_all"],
        )
        print("Finished querying data!")
    elif mode == "train":
        print("starting training setup")
        train(cfg)
    elif mode == "segment":
        print("starting segmentation process")
        if cfg.INFERENCE.MODEL_PATH == "":
            print(
                "You are trying to run the segmentation with no trained model specified, " + \
                  "this will result in random segmentations! Please specify an appropriate model path" +
                  "in your cfg under the option 'INFERENCE.MODEL_PATH' and run this script again."
            )
        else:
            model = BGTsegmentationModel.load_from_checkpoint(cfg.INFERENCE.MODEL_PATH)
            model.eval()
            model = model.to("cuda:0")

            from_h5 = not cfg.TRAIN.DATA_FROM_GEOTIFFS
            segment_folder(
                cfg,
                model,
                from_h5=from_h5,
                save_labels=False,
                segment_only=cfg.KEY_SUBSET,
            )

    elif mode == "postprocess":
        print("starting postprocessing ")
        postprocess(cfg)


if __name__ == "__main__":
    main()
