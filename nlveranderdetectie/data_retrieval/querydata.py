import argparse
import glob
import os
import subprocess
import time
from multiprocessing import Pool
from zipfile import ZipFile

import imageio
import numpy as np
import rasterio
import requests
import wget
from osgeo import gdal
from owslib.wms import WebMapService
from rasterio.crs import CRS
from tqdm import tqdm

from nlveranderdetectie.config import DATA_URLS, REGION_DICT


def request_tile(save_dir, coordinates, data_type, version):
    """request 1km tile of spatial data. See sources.py for options.

    Args:
        save_dir (str): where to save the tile
        coordinates (list): list of coordinates that describe the tile, in format: [x1, y1, x2, y2]
        data_type (str): datatype to request, ie rgb/cir/ahn3
        version (str): year or version of the data to request
    """
    os.makedirs(save_dir, exist_ok=True)

    # query ahn sources
    if DATA_URLS[data_type] == "dsm" or DATA_URLS[data_type] == "dtm":
        request_lr_tile(save_dir, coordinates, data_type, version)

    # query high resolution rgb sources
    elif DATA_URLS[data_type]["layer_resolution"][version][0] >= 2000:
        # hr tiles need to be queried in parts because the wms service wont allow requesting 1km tiles of such resolution.
        request_hr_tile(save_dir, coordinates, data_type, version)

    # query low resolution rgb sources
    else:
        request_lr_tile(save_dir, coordinates, data_type, version)


def request_bgt_tile(save_dir, coordinates, version, classes, bgt_timestamp):
    """Request 1km tile of bgt data and process to geotiff format

    Args:
        save_dir (str): where to save the file.
        coordinates (list): list of coordinates that describe the tile, in format: [x1, y1, x2, y2]
        version (str): version of the data to request, for bgt this means the resolution that the shapes are rasterized to
        classes (list): bgt classes to retrieve
    """
    [x1, y1, x2, y2] = coordinates
    geofilter = "POLYGON(({} {}, {} {}, {} {}, {} {}, {} {}))".format(
        x1, y1, x1, y2, x2, y2, x2, y1, x1, y1
    )

    # build request to pdok api
    response = requests.post(
        DATA_URLS["bgt"]["url"],
        json={"featuretypes": classes, "format": "gmllight", "geofilter": geofilter},
    )
    response_dict = response.json()
    download_id = response_dict["downloadRequestId"]

    # check if the download is ready
    download_ready = False
    status = None
    while not download_ready:
        status = requests.get(
            "https://api.pdok.nl/lv/bgt/download/v1_0/full/custom/{}/status".format(
                download_id
            )
        ).json()
        time.sleep(2)
        if status["status"] == "COMPLETED":
            try:
                download_ready = True
            except:
                pass

    key = f"{int(x1/1000)}_{int(y1/1000)}"
    zip_file_path = os.path.join(save_dir, key, key + ".zip")
    os.makedirs(os.path.join(save_dir, key), exist_ok=True)

    if not os.path.exists(zip_file_path):
        wget.download(
            "https://api.pdok.nl" + status["_links"]["download"]["href"], zip_file_path
        )

    with ZipFile(zip_file_path, "r") as zipObj:
        zipObj.extractall(os.path.join(save_dir, key))

    bgt_gmls = glob.glob(os.path.join(save_dir, key, "*.gml"))
    
    
    for bgt_gml in bgt_gmls:
        target_shapefile = bgt_gml[:-4] + ".shp"
        target_tif = bgt_gml[:-4] + ".tif"
        # translate gml to shp and clip shp
        if bgt_timestamp:
            gdal.VectorTranslate(
                target_shapefile,
                bgt_gml,
                options=f'-f "ESRI Shapefile" -where "(objectEindTijd IS NULL OR CAST(objectEindTijd as date) >= CAST(\'{bgt_timestamp}\' as date)) AND cast(objectBeginTijd as date) <= cast(\'{bgt_timestamp}\' as date) " -spat {str(x1)}, {str(y1)}, {str(x2)}, {str(y2)} -clipsrc spat_extent',

            )
        else:
            gdal.VectorTranslate(
                target_shapefile,
                bgt_gml,
                options=f'-f "ESRI Shapefile" -where "eindRegistratie IS NULL" -spat {str(x1)}, {str(y1)}, {str(x2)}, {str(y2)} -clipsrc spat_extent',
            )
        (size_x, size_y) = DATA_URLS["bgt"]["layer_resolution"][version]

        # rasterize shapefiles
        gdal.Rasterize(
            target_tif,
            target_shapefile,
            options=f"-burn 1.0 -a_srs epsg:28992 -ts {size_x} {size_y} -a_nodata 0 -te {str(x1)}, {str(y1)}, {str(x2)}, {str(y2)} -ot Byte -of GTiff -co COMPRESS=LZW -co TILED=YES",
        )

    # remove leftover and intermediate files
    to_remove = glob.glob(os.path.join(save_dir, key) + "/*.zip")
    to_remove += glob.glob(os.path.join(save_dir, key) + "/*.shp")
    to_remove += glob.glob(os.path.join(save_dir, key) + "/*.gfs")
    to_remove += glob.glob(os.path.join(save_dir, key) + "/*.shx")
    to_remove += glob.glob(os.path.join(save_dir, key) + "/*.prj")
    to_remove += glob.glob(os.path.join(save_dir, key) + "/*.dbf")
    to_remove += glob.glob(os.path.join(save_dir, key) + "/*.gml")

    for file in to_remove:
        os.remove(file)


def request_lr_tile(save_dir, coordinates, data_type, version):
    """Request low resolution tile, lower resolution tiles can be requested directly in one image from the wms services

    Args:
        save_dir (str): where to save the file
        coordinates (list): list of coordinates that describe the tile, in format: [x1, y1, x2, y2]
        data_type (list): data_type == "ahn"
        version (str): version of the data to request, ie. year or in case of ahn3, the resolution
    """
    wms = WebMapService(DATA_URLS[data_type]["url"])
    [x1, y1, x2, y2] = coordinates

    if data_type == "ahn":
        format = "image/GeoTiff"
        is_ahn_query = True
    else:
        format = "image/jpeg"
        is_ahn_query = False

    try:
        img = wms.getmap(
            layers=[DATA_URLS[data_type]["layers"][version]],
            srs="EPSG:28992",
            bbox=(x1, y1, x2, y2),
            size=DATA_URLS[data_type]["layer_resolution"][version],
            format=format,
            transparent=False,
        )
    except requests.exceptions.HTTPError:
        time.sleep(1)
        request_lr_tile(save_dir, coordinates, data_type, version)
        return

    x_left, y_bot, x_right, y_top = (
        int(coordinates[0] / 1000),
        int(coordinates[1] / 1000),
        int(coordinates[2] / 1000),
        int(coordinates[3] / 1000),
    )

    jpg_path = os.path.join(save_dir, f"{x_left}_{y_bot}.jpg")
    tif_path = os.path.join(save_dir, f"{x_left}_{y_bot}.tif")

    if is_ahn_query:
        out = open(tif_path, "wb")
        out.write(img.read())
        out.close()
    else:
        out = open(jpg_path, "wb")
        out.write(img.read())
        out.close()
        array = imageio.imread(jpg_path)

        shape = DATA_URLS[data_type]["layer_resolution"][version]

        scale = 1000 / shape[0]
        transform = rasterio.transform.from_origin(
            int(x_left) * 1000, int(y_top) * 1000, scale, scale
        )
        wkt_crs = CRS.from_epsg(28992)

        new_dataset = rasterio.open(
            tif_path,
            "w",
            driver="GTiff",
            height=shape[0],
            width=shape[1],
            count=3,
            dtype=np.uint8,
            crs=wkt_crs,
            compress="lzw",
            predictor=2,
            transform=transform,
        )
        array = np.moveaxis(array, -1, 0)
        new_dataset.write(array.astype(np.uint8))
        new_dataset.close()

    # remove jpg
    os.remove(jpg_path)


def request_hr_tile(save_dir, coordinates, data_type, version):
    """Request high resolution tile. Tiles from high resolution data sources have to be queried in parts, this function
       will split the entire area up in 16 segments that are requested individually and later merged into a single large file.

    Args:
        save_dir (str): where to save the file
        coordinates (list): list of coordinates that describe the tile, in format: [x1, y1, x2, y2]
        data_type (list): type of data to request, so far only rgb is available in high resolution
        version (str): version of the data to request, ie. year or in case of ahn3, the resolution
    """
    url = DATA_URLS[data_type]["url"]

    layer_name = DATA_URLS[data_type]["layers"][version]
    square_size = DATA_URLS[data_type]["layer_resolution"][version]
    [x1, y1, x2, y2] = coordinates

    bottom_left = (x1, y1)

    final_tif_path = os.path.join(save_dir, f"{int(x1 / 1000)}_{int(y1 / 1000)}.tif")
    vrt_path = os.path.join(save_dir, f"{x1}_{y1}.vrt")

    glob_path = os.path.join(save_dir, "_*.tif")

    # remove leftover old temp files
    glob_path_old_tmps = os.path.join(save_dir, "_*.*")
    part_files = glob.glob(glob_path_old_tmps)
    for fp in part_files:
        os.remove(fp)

    # request HR data in parts, see old querydata.py script
    queries = build_querylist(bottom_left, save_dir, square_size, url, layer_name)

    # do queries asyncronously
    with Pool(1) as p:
        p.map(_do_rgb_query, queries)

    part_files = glob.glob(glob_path)

    gdal.BuildVRT(vrt_path, part_files)

    # could not get gdal.Translate to work properly as it doesnt want to creat files and adding all the gdal.dataset creation
    # options would only make this messier
    command = [
        "gdal_translate",
        "-of",
        "GTiff",
        "-co",
        "COMPRESS=LZW",
        "-co",
        "predictor=2",
        "-co",
        "TILED=YES",
        vrt_path,
        final_tif_path,
    ]
    subprocess.run(command)

    # remove unneeded files:
    os.remove(vrt_path)
    for tif_part_file in part_files:
        os.remove(tif_part_file)
        os.remove(tif_part_file[:-4] + ".jpg")


def build_orthHR_query(
    botleft, xoffset, yoffset, fname, basedir, resolution, url, layer_name
):
    """Build argumentlist for doing a query for a part of a full 1km tile for a high resolution datasource

    Args:
        botleft (tuple): coordinates of the bottom left of the area to request
        xoffset (int): offset in RD for the x coordinate
        yoffset (int): offset in RD for the y coordinate
        fname (str): filename to save the result as
        basedir (str): where to save the result
        resolution (float): amount of pixels in 1m of physical space
        url (str): url of wms service to use
        layer_name (str): layer to query from the wms service

    Returns:
        tuple: tuple containing arguments in following order: (extend, shape, filename, basedir, service_url, layer_name, resolution)
    """
    return (
        [botleft[0], botleft[1], botleft[0] + xoffset, botleft[1] + yoffset],
        (int(xoffset * resolution), int(yoffset * resolution)),
        fname,
        basedir,
        url,
        layer_name,
        resolution,
    )


def build_querylist(botleft, basedir, square_size, url, layer_name):
    """
    construct queries for a single 1x1km tile for the 10cm luchtfoto data. A full 1x1km query for high resolution sources is too big to
    to query at once. Instead multiple smaller queries are done and then merged into a single larger file afterwards.
    The tile is queried in 16 segments of 100mx100m. The smaller images are removed after the query is finished.
    Args:
        botleft: list of two ints representing the x and y coordinates of the tile to be queried in meter coordinates,
                 example: botleft = [122000, 485000]
        basedir: directory where the queried images are saved.

    Returns:
        queries: list of tuples containing arguments for _do_rgb_query
    """

    x_left = botleft[0]
    y_bot = botleft[1]

    # amount of pixels in 1m of physical space
    resolution = 10 / (10000 / square_size[0])

    queries = []
    for x in range(5):
        for y in range(5):
            query = build_orthHR_query(
                [x_left + (200 * x), y_bot + (200 * y)],
                200,
                200,
                f"_{x_left}_{y_bot}_orthHR_{x}{y}.jpg",
                basedir,
                resolution,
                url,
                layer_name,
            )
            queries.append(query)

    return queries


def _do_rgb_query(query):
    """
    Make a single query using a list of arguments generated by build_orth10_query. Requires pem certificate and ssh key,
    you'll have to supply those yourself and put the paths in the function. Alternatively, use the --orth10=False argument
    Args:
        query: list of arguments in format (see build_orthHR_query):

    """
    extend = query[0]
    shape = query[1]
    filename = query[2]
    basedir = query[3]
    service_url = query[4]
    layer_name = query[5]
    resolution = query[6]

    wms = WebMapService(query[4])
    layer = query[5]

    try:
        img = wms.getmap(
            layers=[layer],
            srs="EPSG:28992",
            bbox=extend,
            size=shape,
            format="image/jpeg",
            transparent=False,
        )
    except requests.exceptions.HTTPError:
        time.sleep(1)
        _do_rgb_query(query)
        return

    jpg_path = os.path.join(basedir, filename)
    tmp_tiff_path = os.path.join(basedir, filename[:-4] + ".tif")

    out = open(jpg_path, "wb")
    out.write(img.read())
    out.close()
    time.sleep(1)

    # read image back as np array
    array = np.array(imageio.imread(jpg_path)).astype(np.uint8)
    wkt_crs = CRS.from_epsg(28992)

    # RD SIZE / pixels per meter * meters
    scale = 1000 / (resolution * 1000)
    transform = rasterio.transform.from_origin(extend[0], extend[3], scale, scale)

    # write to georeferenced tiff
    new_dataset = rasterio.open(
        tmp_tiff_path,
        "w",
        driver="GTiff",
        height=query[1][0],
        width=query[1][1],
        count=3,
        dtype=np.uint8,
        crs=wkt_crs,
        compress="lzw",
        transform=transform,
    )

    array = np.moveaxis(array, -1, 0)
    new_dataset.write(array.astype(np.uint8))


def make_coordinate_sets(area):
    """make list of coordinate sets used for querying tiles

    Args:
        area (list): list in format: [x1, y1, x2, y2]

    Returns:
        list: list of coordinate sets
    """
    [x1, y1, x2, y2] = area
    coordinate_sets = []
    for x in range(x1, x2):
        for y in range(y1, y2):
            coordinate_sets.append((x * 1000, y * 1000, (x + 1) * 1000, (y + 1) * 1000))
    return coordinate_sets


def query_data(
    save_dir,
    data_type,
    classes,
    data_version,
    named_area,
    area_from_file,
    bgt_timestamp,
    no_ssl_check=False,
    refresh_all=False,
):
    if no_ssl_check:
        import ssl

        ssl._create_default_https_context = ssl._create_unverified_context

    coordinate_sets = []
    from random import shuffle

    if named_area is not None:
        coordinate_sets = make_coordinate_sets(REGION_DICT[named_area])
    elif area_from_file is not None:
        with open(area_from_file, "r") as coords_file:
            for line in coords_file.readlines():
                patch = [int(c) for c in line.split(",")]
                coordinate_sets += make_coordinate_sets(patch)

    shuffle(coordinate_sets)

    for coord_set in tqdm(coordinate_sets):
        # query bgt sources
        if data_type == "bgt":
            [x1, y1, x2, y2] = coord_set
            target_filepaths = []
            for classname in classes:
                target_filename = f"bgt_{classname}.tif"
                target_filepath = os.path.join(
                    save_dir, f"{int(x1/1000)}_{int(y1/1000)}", target_filename
                )
                target_filepaths.append(target_filepath)

            if (
                not all([os.path.exists(fp) for fp in target_filepaths])
                and not refresh_all
            ):
                request_bgt_tile(
                    save_dir, coord_set, data_version, classes, bgt_timestamp
                )

        # query tile sources
        else:
            target_filename = (
                f"{int(coord_set[0] / 1000)}_{int(coord_set[1] / 1000)}.tif"
            )
            target_file_path = os.path.join(save_dir, target_filename)
            if not os.path.exists(target_file_path) or refresh_all:
                request_tile(save_dir, coord_set, data_type, data_version)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="query data from PDOK")
    parser.add_argument("--save-dir", required=True, help="where to store data")
    parser.add_argument(
        "--data-type",
        required=True,
        default="rgb",
        choices=["rgb", "cir", "dsm", "dtm", "bgt", "ahn3"],
        help="Overwrite existing copies of already queried data.",
    )
    parser.add_argument(
        "--data-version",
        required=True,
        help="What year of data to query or version in case of dsm/dtm.",
    )
    parser.add_argument(
        "--named-area",
        required=False,
        type=str,
        default=None,
        help="named region to query, see sources.py for full list.",
    )
    parser.add_argument(
        "--refresh-all",
        action="store_true",
        default=False,
        help="Overwrite existing copies of already queried data.",
    )
    parser.add_argument(
        "--no-ssl-check",
        action="store_true",
        default=False,
        help="Ignore ssl verification checks, turn on if you get self signed certificate problems.",
    )
    parser.add_argument(
        "--classes",
        default=[
            "pand",
            "waterdeel",
            "gebouwinstallatie",
            "overigbouwwerk",
            "overbruggingsdeel",
        ],
    )
    parser.add_argument(
        "--from-file",
        default=None,
        help="read coordinates from file containing x1,y1,x2,y2 per line",
    )
    parser.add_argument(
        "--bgt-timestamp",
        default=None,
        type=str,
        help="Timestamp to select BGT attributes that where active at that moment.",
    )
    arguments = parser.parse_args()
    query_data(arguments)
