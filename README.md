# NL veranderdetectie

![banner](data/images/banner.png)
![GitLab](https://img.shields.io/gitlab/license/hetwaterschapshuis/kenniscentrum/tooling/nlveranderdetectie)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://gitlab.com/hetwaterschapshuis/kenniscentrum/tooling/nlveranderdetectie/-/blob/master/README.md)

# Introduction

Welcome to the NL veranderdetectie repository. NL veranderdetectie is a project of the Dutch Waterschappen aimed at improving the ease of maintenance of the national BGT map (Basisregistratie Grootschalige Topografie) by automating the detection of changes in the landscape. 

## Table of Contents

- [Background](#background)
- [Data](#data)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)


## Background
This repository contains code to apply the change detection (veranderdetectie) by making of use of deep-learning based segmentation models.
Specifically a segmentation model is trained on large amounts of aerial imagery data and using a selection of BGT labels as target labels. This model is then applied back on the aerial imagery data and the resulting segmentation map is compared to the ground truth. These differences are further filtered and finally used as assistance to BGT maintainers to correct the actual BGT map.

The code is written to allow for the processing of very large (10000+ sqkm) areas. As such it is fairly complex. In this document I shall describe the basic use of the scripts but your own needs may differ from this, requiring a deeper understanding of the underlying codebase. I hope however that this introduction provides enough information to apply the model to your own data. 

## Data
Open access aerial data from the dutch government is available at [pdok.nl](https://www.pdok.nl/datasets) and this repository contains code to retrieve the desired data programatically (see [Usage](#usage)). You can also provide your own data directly by pointing the configs to the right folders (again, see [Usage](#usage))


## Install
This repository depends upon a large amount of packages such as the common numpy, pandas and PIL packages. We provide a operating system agnostic conda environment file to install all dependencies. However, this might not always work perfectly depending on your specific environment. 
Generally the trickiest to get working in a new environment is GDAL. In case you have to build up your own environment, I recommend creating a new (conda) environment and first installing GDAL and pytorch+pytorch-lightning (which is used for the model and training etc.). And then working through the rest of the packages.

The default way to install the package is to clone the package, make a new environment from the `environment.yml` file and then install the nlveranderdetectie package.
```
# clone the package
git clone git@gitlab.com:hetwaterschapshuis/kenniscentrum/tooling/nlveranderdetectie.git

# go into directory
cd nlveranderdetectie

# make new 3.10 environment with the required packages
conda env create -f environment.yml -n nlver

# activate it
conda activate nlver

# install the main nlveranderdetectie package, I recommend doing a local install to be able to modifiy the code if the need arises
pip install -e .
```

An environment file, `environment.yml`, is provided with the repository, but due to gdal and the use of cuda related packages these do not always provide the full portability that one would desire, so I would recommend trying them once and then moving on to installing everything from scratch in a new environment if that doesn't work.

A dockerfile will also be provided in the near future to solve this portability issue.

## Usage
After setting up the environment you can call the package in the command line with `nlver` to perform various functions. There are four main modules in the package, accessed through four modes given as command line arguments:
1. Data Retrieval: `query`
2. Training: `train`
3. Inference: `segment`
4. Postprocess: `postprocess`

### 1. The Config 
All the configurations for the package, including paths to store the data, parameters of the model and all other options are described in a config file. Example files are provided in the `run/configs/` folder. The config works by overwriting default values in the `nlveranderdetectie/config/default.py` script. A full description of each config option is available there and any new options must be added there as well. 

When starting the pipeline for a new area, make a new config based on one of the defaults and adjust the filepaths to the data, the number of desired classes, the model type, the training setup and anything else specific to your run. After setting up this file the various functionalities of the package can be accessed by passing your config. 

Some modes might require additional arguments, this is because you might want to run multiple variants of the same config or various other reasons.

### 1. Data retrieval
#### 1.1 Using query_data.py
The first step in the process is to retrieve the data from PDOK. Alternatively it is also possible to supply your own data by formatting in the correct format. For more information on that see [custom data](#custom-data).
The command: `nlver --cfg path/to/cfg.yml --mode query` can be used to retrieve data from PDOK, read the arguments carefully to see which source can be retrieved with which name.
The script must be run for every data source that you want to use, so in the most basic usecase you'll have to run it for the RGB aerial data and the BGT label data.

The configuration for the data retrieval; The links, names and areas. can be found in `data_retrieval/sources.py`. If you want to specify a specific area to query you can do so by adding it there as well. 

In the most simple usecase, you would only want ot retrieve the rgb data and bgt label data. <br>
Example:
```
# retrieve rgb
nlver --cfg run/configs/fryslan_example.yml --mode query --data-version current --data-type rgb --named-area fryslan_pilot1

# retrieve bgt
nlver --cfg run/configs/fryslan_example.yml --mode query --data-version 8cm --data-type bgt --named-area fryslan_pilot1 --bgt-timestamp="2023-01-01"
```

These two calls will retrieve the RGB and BGT label data. Adjust the config (see [config](#the-config)) to point the script to the correct locations. It is also possible to add additional arguments to this call using the `--opts` argument to overwrite any option in the config file for example the target save directory. This is useful if you want to have multiple versions of say the rgb data, but dont want to duplicate the entire `yml` file.
```
# retrieve current rgb
nlver --cfg run/configs/fryslan_example.yml --mode query --data-version current --data-type rgb --named-area fryslan_pilot1

# retrieve rgb from 2018
nlver --cfg run/configs/fryslan_example.yml --mode query --data-version 2018 --data-type rgb --named-area fryslan_pilot1 --opts DATASET.DATA_SOURCE_FOLDERS.CUSTOM_RGB_DIR "new/path/to/rgb_2018"
```


#### 1.2 Data format
The data is expected to be in the following format:
_Georeferenced_ and _tiled_ geotiffs, named after the top left coordinate in _RD\_NEW_ coordinates. With the realworld size of 1km by 1km.
_example_:
259000_536000.tif, would contain griddata from 259000 to 26000 and 535000 to 536000.
It is essential that the geotiffs are tiled internally because the samples for training will be read form the tifs directly, tiling allows for partial reading of the file instead of only allowing for the complete tile, which would be much to slow for just small patches.

The BGT data is saved slightly differently as it is saved per class. Instead of one tif file, the square kilometer areas are saved in a folder named after the coordinate previously mentions. The classes are then saved in individual tif files named after the class the contain. so for example the folder `259000_536000` would contain tif files: `waterdeel.tif` & `pand.tif`

## 2. Training
The `train` mode can be used to train a segmentation model after you have setup all the data in the required format.
Training is one of the most time intensive steps and there are various options in the config to change the model parameters to your needs in terms of model size and training time. This requires some knowledge of deep learning.

### Logging
[Weights and Biases](https://wandb.ai/) is used to log the training runs and the resulting models/code etc. You can turn it off and do a run without any of that but I suggest creating a weights and biases account and logging in in whatever environment you are running the script. This will allow you to see the progress of your training from anywhere as you can just see it from your browser. Additionally you can stop runs that don't make any progress or have experienced gradient explosions. 

### Running the script
Once you have configured your config file you can run it by simply calling:
```
nlver --cfg run/configs/fryslan_example.yml --mode train
```
Training time varies based on how much data is used, but as a general rule of thumb, let the training continue until the epoch_loss, that is the averge loss of an epoch, does not go down anymore.


## 3. Inference
### 3.1 Segmentation
After you have trained a model on your data type, you can run the segmentation on your data using the `segment` mode. The script uses the same config file as the training, but you will have to add a path to the model you wish to use for the segmentation to the config file,
[example](run/configs/fryslan_example.yml):

```
# example :
INFERENCE :
  BATCH_SIZE : 2
  MODEL_PATH : "/data/models/transf_seg-epoch=21-dice=0.95.ckpt"
```
Call the segmentation mode with:
```
nlver --cfg run/configs/fryslan_example.yml --mode segment
```
The `segment` mode will loop through all files and save the results in a georeferenced tif file in the folder specified in the config.

### 3.2 Postprocessing (optional)
The `segment` mode will generate the segmented versions of the data as geotiffs, but those will still  need a lot of postprocessing to be useful. The `postprocess` mode performs the optional step of comparing the segmented tiles with the ground truth and generating new tiles that only contain the differences between those two for each class and each comparison (false negatives & false positive). 

If you are using a more complicated postprocessing pipeline, you can skip this step and process the segmented tiles directly in the application of your choice.

#### Further Postprocessing
After segmenting the data, you will probably want to postprocess the data further. As there are many options for doing this there is currently no python module in this repository to do this. You can achieve things like merging the files, polygonizing the files, processing the resulting polygons in programs like Arcgis, Qgis, FME or using packages like GDAL. 
Below are some examples of what can be done with GDAL
```
# mergin files using GDAL
## first build the virtual raster
gdalbuildvrt /data/fryslan/difference_map/fp_WATERDEEL.vrt /data/fryslan/difference_map/fp_waterdeel*.tif

# convert the virtual raster into a geotiff.
gdal_translate -of GTiff -co "COMPRESS=LZW" -co "TILED=YES" /data/fryslan/difference_map/fp_WATERDEEL.vrt /data/fryslan/difference_map/fp_WATERDEEL.tif

# polygonize the big geotiff using GDAL
gdal_polygonize.py /data/fryslan/difference_map/fp_WATERDEEL.tif /data/fryslan/difference_map/fp_WATERDEEL.shp difference_map feature

```

#### Note
Over the course of the development of this pacakge, many different methods have been used to complete te pipeline. While the main modes provide streamlined access to the functionality, more functionality is available within the package itself if needed, see for example the `misc_utils` folder.


### Additional processing options
As alternative to using the Geotiff format, the [hdf5](https://www.h5py.org/) format can also be used. The advantage of using hdf5 is that multiple data types can all be stored in a single file, greatly reducing the amount of folders that need to be kept track off. The downside is that if you are working with very large amounts of data, it might not be possible to convert the entire dataset to hdf5 format and effectively double your storage usage. 

In the code you will find references as well to hdf5 being used to store feature maps of the output. The idea is to run the segmentation on different resolution versions of the data in order to incorporate more spatial context into the segmentation. This feature is currently experimental and underdeveloped. Use at your own discretion.

## Contributing
All contributions are welcome! For direct improvements please submit a pull request. Any further questions about contributing or usage can be sent to j.gerbscheid@hetwaterschapshuis.nl. You can of course also open issues on the issue board.

## License

[MIT © Het Waterschapshuis.](./LICENSE)
